
of Éostran healle,   cume þú hál!
hál and hálig,       on heofnas arís;
léoht bring ús tó,   O beorhtost bist þú:
cum þú O Sunne,      and wite sige!

on heofna ráde,                 wes þú ful hál!
hál and hálig                   Héahcyninge:
weorþige man þé O Gyldene Áne   þe wiermþe and gléow tó him gyfest on dæg.
scín þú O Sweglgim,             and wite sige!

morgentíde swá swá efentíde,         middangearde swá swá elfenríce.
awerged wé sténað getoren,           wérgod wé sint á getilod.
wé lecgað ús selfe tó restanne nú,   þéah ríse þú swiðe tó dógore néah;
gá cwice þú ac cum þú gelíce,        þú cwén þára heofna árlíce:
swef þú on sweartunge,               and wite sige!


O noble Ellie, thee we hardly knew
Empress most high, to whom were we true
Rest now in peace, 'til day of doom
When God shall reckon where thou shalt room
Whether bane of hell or ever heaven's boon
The scriptures tell us we'll find out soon

One thing I know is I would thou room with me
In the glow of hell we could be truly free
The Good Shepherd in heaven forever counting sheep
While we party all night with no need for sleep
Eternal infernal fires provide perpetual gleam
And we never need worry we'll run out of steam

